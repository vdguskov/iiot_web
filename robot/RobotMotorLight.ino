int pinD0 = A1;                    // Пин к которому подключен D0
// Присваиваем имя для порта 9 со светодиодом
#define LED 13

#define SPEED_1      5  //колесо перед лево
#define DIR_1        4

#define SPEED_2      6 //колесо перед право 
#define DIR_2        7

#define SPEED_3      8 //колесо зад лево
#define DIR_3        9

#define SPEED_4      3 //колесо зад право
#define DIR_4        2

void setup() 
{
    // настраиваем выводы платы 4, 5, 6, 7 на вывод сигналов 
  for (int i = 4; i <8; i++) {     
    pinMode(i, OUTPUT);
  }
  // Пин 9 со светодиодом будет выходом (англ. «output»)
  pinMode(LED, OUTPUT);
  pinMode (pinD0, INPUT);          // Установим вывод A1 как вход
  Serial.begin (9600);             // Задаем скорость передачи данных
}

void loop() 
{
  int xD0;                     // Создаем переменные
  xD0 = digitalRead (pinD0);        // считываем значение с порта pinD0

  Serial.print("Sensor: ");         // Выводим текст
    
  if (xD0 == HIGH)                  // Если xD0 равно "1" 
   {            
     Serial.println ("ON");         // Выводим текст
     digitalWrite(LED, HIGH);
        analogWrite(SPEED_1, 0);
  analogWrite(SPEED_2, 0);
  analogWrite(SPEED_3, 0);
  analogWrite(SPEED_4, 0);
    // устанавливаем направление моторов в другую сторону
  digitalWrite(DIR_1, LOW);//назад
  digitalWrite(DIR_2, HIGH);//назад
  digitalWrite(DIR_3, HIGH);//назад 
  digitalWrite(DIR_4, HIGH);//вперед
   }
  else
   {
    Serial.println ("OFF");         // Если xD0 равно "0" 
    digitalWrite(LED, LOW);
       // устанавливаем направление мотора «M1» в одну сторону
  digitalWrite(DIR_1, HIGH); //назад
  // включаем мотор на максимальной скорости
  analogWrite(SPEED_1, 255);
    // устанавливаем направление мотора «M2» в одну сторону
  digitalWrite(DIR_2, LOW);//вперед
  // включаем мотор на максимальной скорости
  analogWrite(SPEED_2, 255); //
  // устанавливаем направление мотора «M3» в одну сторону
  digitalWrite(DIR_3, LOW);//назад
  // включаем мотор на максимальной скорости
  analogWrite(SPEED_3, 255);
    // устанавливаем направление мотора «M4» в одну сторону
  digitalWrite(DIR_4, LOW);//вперед
  // включаем мотор на максимальной скорости
  analogWrite(SPEED_4, 255);   }
delay (500);                        // Ждем 500 мкс.
}
